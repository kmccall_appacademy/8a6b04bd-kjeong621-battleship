class BattleshipGame
  attr_reader :board, :player
  def initialize(player, board)
    @player = player
    @board = board
  end

  def play
    until game_over?
      @current_player.play_turn
    end
    puts "Game over, you win!"
  end

  def attack(position)
    @board[position] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    pos = player.get_play
    attack(pos)
  end
end
